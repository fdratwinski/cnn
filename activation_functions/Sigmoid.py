from activation_functions.ActivationFunction import ActivationFunction
import numpy as np


class Sigmoid(ActivationFunction):
    def calculate_activation(self, x):
        return 1 / (1 + np.exp(-x))

    def calculate_derivation(self, x):
        sigmoid_value = self.calculate_activation(x)
        return sigmoid_value * (1 - sigmoid_value)
