from activation_functions.ActivationFunction import ActivationFunction
import numpy as np


class ReLU(ActivationFunction):
    def calculate_activation(self, x):
        return np.maximum(x, 0)

    def calculate_derivation(self, x):
        x[x <= 0] = 0
        x[x > 0] = 1
        return x
