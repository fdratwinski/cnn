import math

import numpy as np


class Network:

    def __init__(self, num_of_inputs, filters_sizes, num_of_neurons_hidden, num_of_classes,
                 activation_functions, optimizer, learning_rate):
        self.learning_rate = learning_rate
        self.num_of_classes = num_of_classes
        self.activation_functions = activation_functions
        self.optimizer = optimizer
        self.filters_sizes = filters_sizes
        self.filters = []
        self.filters.append(
            np.random.uniform(-5, 5,
                              size=(self.filters_sizes[0][0] * self.filters_sizes[0][1], self.filters_sizes[0][2],
                                    self.filters_sizes[0][3])))
        self.filters.append(
            np.random.uniform(-5, 5,
                              size=(self.filters_sizes[1][0] * self.filters_sizes[1][1], self.filters_sizes[1][2],
                                    self.filters_sizes[1][3])))
        self.W_list = []
        self.b_list = []

        self.num_of_neurons_hidden = num_of_neurons_hidden
        value = 2 / num_of_inputs
        self.W_list.append(
            np.random.uniform((-1) * value, value, (num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            value = 2 / (num_of_neurons_hidden[i])
            self.W_list.append(np.random.uniform((-1) * value, value,
                                                 (num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.b_list.append(np.zeros(num_of_neurons_hidden[i]))

        value = 2 / (num_of_neurons_hidden[-1])
        self.W_list.append(
            np.random.uniform((-1) * value, value, (num_of_neurons_hidden[-1], num_of_classes)))
        self.b_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_list.append(np.zeros(num_of_classes))

        self.a = []
        self.z = []
        self.best_loss = None
        self.bestW = None
        self.bestB = None

    def convolve(self, inputs, filter, padding):
        filter_size = int(math.sqrt(filter.shape[0]))
        if padding > 0:
            inputs = np.pad(inputs, pad_width=[(0, 0), (padding, padding), (padding, padding), (0, 0)],
                            constant_values=0)
        before_result = np.zeros(shape=(
            inputs.shape[0], inputs.shape[1] - filter_size + 1, inputs.shape[2] - filter_size + 1,
            inputs.shape[3] * filter_size * filter_size))
        for i in range(0, filter_size):
            for j in range(0, filter_size):
                before_result[:, :, :,
                (i * filter_size + j) * inputs.shape[3]:((i * filter_size + j) * inputs.shape[3]) + inputs.shape[
                    3]] = inputs[:, i:before_result.shape[1] + i,
                          j:before_result.shape[2] + j, :]

        return before_result @ filter.reshape(filter.shape[0] * filter.shape[1], filter.shape[2])

    def convolve_backprop(self, inputs, filter, filter_size):
        filter_update = np.zeros(shape=filter_size)

        for j in range(filter_update.shape[0]):
            for k in range(filter_update.shape[1]):
                filter_update[j, k, :] += np.sum(
                    inputs[:, :, :, :] * filter[:, j:j + inputs.shape[1], k:k + inputs.shape[2]], axis=(0, 1, 2))

        return filter_update

    def forward_pass_step(self, weights, bias, inputs, activation_func):
        full_impulse = inputs @ weights + bias
        return activation_func.calculate_activation(full_impulse), full_impulse

    def forward_pass(self, data):
        self.a.clear()
        self.z.clear()
        data = np.expand_dims(data, axis=4)

        first_convolve = self.convolve(data, self.filters[0], 0)
        self.z.append(first_convolve)
        first_convolve_act = self.activation_functions[0].calculate_activation(first_convolve)
        self.a.append(first_convolve_act)

        second_convolve = self.convolve(first_convolve_act, self.filters[1], 0)

        flatten = np.reshape(second_convolve, newshape=(
            second_convolve.shape[0], second_convolve.shape[1] * second_convolve.shape[2] * second_convolve.shape[3]))

        self.z.append(flatten)
        flatten = self.activation_functions[1].calculate_activation(flatten)
        self.a.append(flatten)

        output, impulse = self.forward_pass_step(self.W_list[0], self.b_list[0], flatten, self.activation_functions[2])
        self.a.append(output)
        self.z.append(impulse)
        for i in range(1, len(self.W_list)):
            output, impulse = self.forward_pass_step(self.W_list[i], self.b_list[i], self.a[-1],
                                                     self.activation_functions[2 + i])
            self.a.append(output)
            self.z.append(impulse)

        return np.argmax(self.a[-1], axis=1)

    def back_propagation(self, data, actual_labels):
        true_labels_one_hot = self.to_one_hot_encoding(actual_labels, self.num_of_classes)
        shape = data.shape[0]
        deltas = []

        delta = self.activation_functions[-1].calculate_derivation(self.z[-1]) * (
                true_labels_one_hot - self.a[-1])
        deltas.append(delta)
        self.update_weights(delta, -1, shape, self.a[-2])

        for i in range(1, len(self.W_list) - 1):
            delta = self.activation_functions[-1 - i].calculate_derivation(self.z[-1 - i]) * (
                    deltas[-1] @ self.W_list[-1 * i].T)
            deltas.append(delta)
            self.update_weights(delta, -1 - i, shape, self.a[-2 - i])

        delta = self.activation_functions[2].calculate_derivation(self.z[2]) * (deltas[-1] @ self.W_list[1].T)
        deltas.append(delta)
        self.update_weights(delta, 0, shape, self.a[1])

        delta = self.activation_functions[1].calculate_derivation(self.z[1]) * (deltas[-1] @ self.W_list[0].T)
        delta_size = int(math.sqrt(delta.shape[1]))
        delta = np.reshape(delta, newshape=(delta.shape[0], delta_size, delta_size, 1))
        deltas.append(delta)

        back_prop_value = self.convolve_backprop(self.a[0], delta, (1, 1, self.a[0].shape[3]))
        back_prop_value = np.reshape(back_prop_value, newshape=self.filters[1].shape)
        self.filters[1] += self.learning_rate * back_prop_value / data.shape[0]

        delta = self.convolve(deltas[-1], self.filters[0], 2) * self.activation_functions[0].calculate_derivation(
            self.z[0])
        deltas.append(delta)
        data_exp = np.expand_dims(data, axis=4)

        filter_size_sqrt = int(math.sqrt(self.filters[0].shape[0]))
        value_convolve_backprop = self.convolve_backprop(delta, data_exp,
                                                         (filter_size_sqrt, filter_size_sqrt, self.filters[0].shape[2]))
        rot180 = np.rot90(np.rot90(value_convolve_backprop,axes=(0,1)),axes=(0,1))
        rot180 = np.reshape(rot180,newshape=(filter_size_sqrt*filter_size_sqrt, 1, self.filters[0].shape[2]))
        self.filters[0] += self.learning_rate * rot180 / data.shape[0]


    def update_weights(self, delta, index, shape, output):
        self.optimizer.update_weights(self.W_list, self.b_list, delta, index, shape, output)

    def calculate_accuracy_and_loss(self, x, y):
        predicted_labels = self.forward_pass(x)
        last_output = self.a[-1]
        loss = np.sum((last_output - self.to_one_hot_encoding(y, self.num_of_classes)) ** 2) / x.shape[0]
        return np.sum(predicted_labels == y) / len(y), loss

    def check_for_best_loss(self, loss):
        if (self.best_loss is not None and loss < self.best_loss) or (self.best_loss is None):
            print(f"\tSaving best weights. Best loss was {self.best_loss}, new loss is {loss}")
            self.best_loss = loss
            self.bestW = self.W_list.copy()
            self.bestB = self.b_list.copy()

    def to_one_hot_encoding(self, labels, num_of_classes):
        labels_one_hot = np.zeros(shape=(labels.shape[0], num_of_classes))
        for i in range(len(labels)):
            labels_one_hot[i][labels[i]] = 1
        return labels_one_hot
