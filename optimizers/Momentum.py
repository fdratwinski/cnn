from optimizers.Optimizer import Optimizer

import numpy as np


class Momentum(Optimizer):
    def __init__(self, learning_rate, gamma, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        super().__init__(learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes)
        self.gamma = gamma
        self.last_W_change = []
        self.last_b_change = []

        self.last_W_change.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            self.last_W_change.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.last_b_change.append(np.zeros((num_of_neurons_hidden[i])))

        self.last_W_change.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.last_b_change.append(np.zeros(num_of_neurons_hidden[-1]))
        self.last_b_change.append(np.zeros(num_of_classes))

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        W_change = self.learning_rate * (output.T @ delta / shape) + self.gamma * self.last_W_change[index]
        b_change = self.learning_rate * (delta.T @ (np.ones(shape)) / shape) + self.gamma * self.last_b_change[index]
        W_list[index] += W_change
        b_list[index] += b_change

        self.last_W_change[index] = W_change
        self.last_b_change[index] = b_change

    def get_name(self):
        return "Momentum"

